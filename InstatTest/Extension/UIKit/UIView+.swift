//
//  UIView+.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import UIKit

extension UIView {
    class var identifier: String {
        return String(describing: self)
    }
}
