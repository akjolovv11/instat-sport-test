//
//  String+.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation

extension String {
    func toDate(format: String = "yyyy-MM-dd HH:mm") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        
        var strDate = self.replacingOccurrences(of: "T", with: " ")
        strDate = strDate.replacingOccurrences(of: "Z", with: "")
        let _self = strDate.prefix(16)
        let range = strDate.range(of: _self)
        strDate = strDate.replacingOccurrences(of: _self, with: "", options: .regularExpression, range: range)
        
        let date = dateFormatter.date(from: "\(_self)")
        return date
    }
}
