//
//  Date+.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation

extension Date {
    func toString(format: String = "dd.MM.yyyy HH:mm") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
