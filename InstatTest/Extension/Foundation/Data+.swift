//
//  Data+.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation
import UIKit

extension Data {
    func getImage() -> UIImage? {
        if let image = UIImage(data: self) {
            return image
        } else {
            return UIImage(named: "im_placeholder")
        }
    }
}
