//
//  LeaguesPresenter.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

protocol BaseProtocol {
    func presentError(error: NetworkError)
}

protocol LeaguesPresenterDelegate: BaseProtocol, AnyObject {
    func presentAvailableLeagues(leagues: [League])
    func presentLeagueDetail(id: String?)
}

class LeaguesPresenter {
    
    weak private var delegate: LeaguesPresenterDelegate?
    
    func setViewDelegate(delegate: LeaguesPresenterDelegate) {
        self.delegate = delegate
    }
    
    func getAvailableLeagues() {
        LeaguesNetworkManager.shared.getAvailableLeagues { baseLeague in
            self.delegate?.presentAvailableLeagues(leagues: baseLeague.data)
        } onError: { error in
            self.delegate?.presentError(error: error)
        }
    }
    
    func didSelectAvailableLeagues(with id: String?) {
        delegate?.presentLeagueDetail(id: id)
    }
}
