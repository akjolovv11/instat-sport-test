//
//  SeasonsPresenter.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation

protocol SeasonsPresenterDelegate: BaseProtocol, AnyObject {
    func presentSeasons(seasonInfo: SeasonInfo?)
}

class SeasonsPresenter {
    
    weak private var delegate: SeasonsPresenterDelegate?
    
    func setViewDelegate(delegate: SeasonsPresenterDelegate) {
        self.delegate = delegate
    }
    
    func getSeasonsByLeagueId(with id: String) {
        LeaguesNetworkManager.shared.getSeasonsById(id) { baseSeason in
            self.delegate?.presentSeasons(seasonInfo: baseSeason.data)
        } onError: { error in
            self.delegate?.presentError(error: error)
        }
    }
}
