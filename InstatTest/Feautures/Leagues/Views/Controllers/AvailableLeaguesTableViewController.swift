//
//  AvailableLeaguesTableViewController.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import UIKit

class AvailableLeaguesTableViewController: BaseTableViewController {
    
    private let presenter: LeaguesPresenter = appDelegate.assembler.resolve()
    
    private var availableLeagues: [League] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "All Leagues Available"
        setupTableView()
        presenter.setViewDelegate(delegate: self)
        presenter.getAvailableLeagues()
    }
    
    private func setupTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(AvailableLeagueTableViewCell.self, forCellReuseIdentifier: AvailableLeagueTableViewCell.identifier)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableLeagues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AvailableLeagueTableViewCell.identifier, for: indexPath) as! AvailableLeagueTableViewCell
        cell.setupWith(league: availableLeagues[indexPath.row])
        
        MainNetworkManager.shared.getPhoto(url: availableLeagues[indexPath.row].logos?.light ?? "") { data in
            DispatchQueue.main.async {
                if (cell.representedIdentifier == self.availableLeagues[indexPath.row].id) {
                    cell.logoImageView.image = data.getImage()
                }
            }
        } onError: { error in
            print(error.message)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectAvailableLeagues(with: availableLeagues[indexPath.row].id)
    }
}

extension AvailableLeaguesTableViewController: LeaguesPresenterDelegate {
    func presentAvailableLeagues(leagues: [League]) {
        availableLeagues = leagues
    }
    
    func presentError(error: NetworkError) {
        print(error.message)
    }
    
    func presentLeagueDetail(id: String?) {
        let vc: SeasonLeagueTableViewController = appDelegate.assembler.resolve()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }
}
