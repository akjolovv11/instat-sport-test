//
//  SeasonLeagueTableViewController.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import UIKit

class SeasonLeagueTableViewController: BaseTableViewController {
    
    private let presenter: SeasonsPresenter = appDelegate.assembler.resolve()
    
    var id: String?
    
    private var seasonInfo: SeasonInfo? {
        didSet {
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Available League Seasons"
        setupTableView()
        presenter.setViewDelegate(delegate: self)
        getSeasonsById()
    }
    
    @objc private func getSeasonsById() {
        if let id = id {
            presenter.getSeasonsByLeagueId(with: id)
        }
    }
    
    private func setupTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(SeasonTableViewCell.self, forCellReuseIdentifier: SeasonTableViewCell.identifier)
        
        refreshControl?.addTarget(self, action: #selector(getSeasonsById), for: .valueChanged)
        refreshControl?.beginRefreshing()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let seasons = seasonInfo?.seasons else {
            return 0
        }
        return seasons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SeasonTableViewCell.identifier, for: indexPath) as! SeasonTableViewCell
        cell.setupWith(season: seasonInfo?.seasons[indexPath.row])
        return cell
    }
}

extension SeasonLeagueTableViewController: SeasonsPresenterDelegate {
    func presentSeasons(seasonInfo: SeasonInfo?) {
        self.seasonInfo = seasonInfo
    }
    
    func presentError(error: NetworkError) {
        print(error.message)
    }
}
