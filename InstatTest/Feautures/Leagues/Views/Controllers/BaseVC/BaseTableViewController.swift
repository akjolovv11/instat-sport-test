//
//  BaseTableViewController.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl!)
    }
}

