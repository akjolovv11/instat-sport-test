//
//  SeasonTableViewCell.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import UIKit

class SeasonTableViewCell: UITableViewCell {
    
    let displayNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 5
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let startDateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let endDateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var season: Season? {
        didSet {
            displayNameLabel.text = season?.displayName
            startDateLabel.text = "Start date: " + (season?.startDate?.toDate()?.toString() ?? "")
            endDateLabel.text = "End date: " + (season?.endDate?.toDate()?.toString() ?? "")
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }
    
    func setupWith(season: Season?) {
        self.season = season
    }
    
    private func setupConstraints() {
        addSubview(displayNameLabel)
        displayNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        displayNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        displayNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        
        addSubview(startDateLabel)
        startDateLabel.topAnchor.constraint(equalTo: displayNameLabel.bottomAnchor, constant: 8).isActive = true
        startDateLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        startDateLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        
        addSubview(endDateLabel)
        endDateLabel.topAnchor.constraint(equalTo: startDateLabel.bottomAnchor, constant: 8).isActive = true
        endDateLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        endDateLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        endDateLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

