//
//  AvailableLeagueTableViewCell.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import UIKit

class AvailableLeagueTableViewCell: UITableViewCell {

    let nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 5
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
        
    let abbreviationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var representedIdentifier: String?
    
    var league: League? {
        didSet {
            nameLabel.text = league?.name
            abbreviationLabel.text = league?.abbr
            representedIdentifier = league?.id
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }

    func setupWith(league: League) {
        self.league = league
    }
    
    private func setupConstraints() {
        addSubview(logoImageView)
        logoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        logoImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        logoImageView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -16).isActive = true
        
        addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: logoImageView.rightAnchor, constant: 12).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true

        addSubview(abbreviationLabel)
        abbreviationLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8).isActive = true
        abbreviationLabel.leftAnchor.constraint(equalTo: logoImageView.rightAnchor, constant: 12).isActive = true
        abbreviationLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        abbreviationLabel.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -16).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
