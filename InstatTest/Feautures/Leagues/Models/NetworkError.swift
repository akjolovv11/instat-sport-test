//
//  NetworkError.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class NetworkError: Error {

    var statusCode: Int = -1
    var title: String = "Ошибка"
    var message: String = "Произошла неизвестная ошибка"

    static var undefined = NetworkError(title: "Ошибка", message: "Произошла неизвестная ошибка", code: -1)

    init(title: String? = "Ошибка", message: String?, code: Int?) {
        self.title = title ?? "Ошибка"
        self.message = message ?? "Произошла неизвестная ошибка"
        self.statusCode = code ?? -1
    }

    init(_ error: Error? = nil) {
        guard let networkError = error as? NetworkError else {
            title = "Ошибка"
            message = error?.localizedDescription ?? ""
            statusCode = -1
            return
        }
        title = networkError.title
        message = networkError.message
        statusCode = networkError.statusCode
    }
}
