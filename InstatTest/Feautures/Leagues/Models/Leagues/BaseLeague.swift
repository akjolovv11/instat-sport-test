//
//  League.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class BaseLeague: BaseModel {
    
    var data: [League] = []
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let data = try? values.decodeIfPresent([League].self, forKey: .data) {
            self.data = data
        }
        super.init()
    }
}
