//
//  Logo.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class Logo: Codable {
    
    var light: String?
    var dark: String?
    
    private enum CodingKeys: String, CodingKey {
        case light, dark
    }
    
    init() {}
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        light = try? values.decodeIfPresent(String.self, forKey: .light)
        dark = try? values.decodeIfPresent(String.self, forKey: .dark)
    }
}
