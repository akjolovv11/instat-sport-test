//
//  File.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class League: Codable {
    
    var id: String?
    var name: String?
    var slug: String?
    var abbr: String?
    var logos: Logo?
    
    private enum CodingKeys: String, CodingKey {
        case id, name, slug, abbr, logos
    }
    
    init() {}
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decodeIfPresent(String.self, forKey: .id)
        name = try? values.decodeIfPresent(String.self, forKey: .name)
        slug = try? values.decodeIfPresent(String.self, forKey: .slug)
        abbr = try? values.decodeIfPresent(String.self, forKey: .abbr)
        logos = try? values.decodeIfPresent(Logo.self, forKey: .logos)
    }
}
