//
//  SeasonCell.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation

class Season: Codable {
    
    var year: Int?
    var startDate: String?
    var endDate: String?
    var displayName: String?
    
    private enum CodingKeys: String, CodingKey {
        case year, startDate, endDate, displayName
    }
    
    init() {}
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        year = try? values.decodeIfPresent(Int.self, forKey: .year)
        startDate = try? values.decodeIfPresent(String.self, forKey: .startDate)
        endDate = try? values.decodeIfPresent(String.self, forKey: .endDate)
        displayName = try? values.decodeIfPresent(String.self, forKey: .displayName)
    }
}
