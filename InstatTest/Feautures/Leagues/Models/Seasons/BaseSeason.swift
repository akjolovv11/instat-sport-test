//
//  BaseSeason.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation

class BaseSeason: BaseModel {
    
    var data: SeasonInfo?
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try? values.decodeIfPresent(SeasonInfo.self, forKey: .data)
        super.init()
    }
}
