//
//  Season.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation

class SeasonInfo: Codable {
    
    var name: String?
    var desc: String?
    var abbreviation: String?
    var seasons: [Season] = []
    
    private enum CodingKeys: String, CodingKey {
        case name, desc, abbreviation, seasons
    }
    
    init() {}
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try? values.decodeIfPresent(String.self, forKey: .name)
        desc = try? values.decodeIfPresent(String.self, forKey: .desc)
        abbreviation = try? values.decodeIfPresent(String.self, forKey: .abbreviation)
        if let seasons = try? values.decodeIfPresent([Season].self, forKey: .seasons) {
            self.seasons = seasons
        }
    }
}
