//
//  Base.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class BaseModel: Codable {
    
    var status: Bool?
    
    private enum CodingKeys: String, CodingKey {
        case status
    }
    
    init() {}
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try? values.decodeIfPresent(Bool.self, forKey: .status)
    }
}
