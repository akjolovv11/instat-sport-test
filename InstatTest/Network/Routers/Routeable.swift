//
//  Routeable.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

public enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

protocol Routeable {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: [String: Any] { get }
    var header: [String: String] { get }

    func toURLRequest() throws -> URLRequest
    func asURLRequest() throws -> URLRequest
}

extension Routeable {

    var header: [String: String] {
        get {
            let parameters: [String: String] = [
                "content-type": "application/json"
            ]
            return parameters
        } set { }
    }

    func asURLRequest() -> URLRequest {
        let urlRequest = try? toURLRequest()
        return urlRequest!
    }

    func toURLRequest() throws -> URLRequest {
        let urlString = "\(Config.shared.baseURL)\(path)"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        var request = URLRequest(url: url!)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = header
        if !parameters.isEmpty {
            let jsonData = try? JSONSerialization.data(withJSONObject: parameters)
            request.httpBody = jsonData
        }
        return request
    }
}
