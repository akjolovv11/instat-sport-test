//
//  LeaguesRouter.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

enum LeaguesRouter: Routeable {
    
    case leagues
    case leagueById(id: String)
    case seasonsByid(id: String)
    
    var method: HTTPMethod {
        switch self {
        case .leagues, .leagueById, .seasonsByid:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .leagues:
            return "leagues"
        case .leagueById(let id):
            return "leagues/\(id)"
        case .seasonsByid(let id):
            return "leagues/\(id)/seasons"
        }
    }
    
    var parameters: [String : Any] {
        switch self {
        case .leagues, .leagueById, .seasonsByid:
            return [:]
        }
    }
}
