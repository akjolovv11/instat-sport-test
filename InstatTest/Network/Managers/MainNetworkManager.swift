//
//  MainNetworkManager.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class MainNetworkManager: BaseNetworkManager {
    
    static let shared = MainNetworkManager()
    private override init() {}
    
    private var images = NSCache<NSString, NSData>()
    
    func getPhoto(url: String,
                  onSuccess: @escaping ((Data) -> Void),
                  onError: @escaping ((NetworkError) -> Void)) {
        if let imageData = images.object(forKey: url as NSString) {
            onSuccess(imageData as Data)
            return
        }
        guard let URL = URL(string: url) else {
            onError(NetworkError(title: "Ошибка", message: "Не правильная ссылка", code: 0))
            return
        }
        
        request(with: URL) { data in
            self.images.setObject(data as NSData, forKey: url as NSString)
            onSuccess(data)
        } onError: { error in
            onError(error)
        }
    }
}
