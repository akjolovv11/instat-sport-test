//
//  NetworkManager.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class LeaguesNetworkManager: BaseNetworkManager {
    
    static let shared = LeaguesNetworkManager()
    private override init() {}
    
    func getAvailableLeagues(onSuccess: @escaping ((BaseLeague) -> Void),
                             onError: @escaping ((NetworkError) -> Void)) {
        let route = LeaguesRouter.leagues.asURLRequest()
        request(with: route) { data in
            if let baseLeague = try? JSONDecoder().decode(BaseLeague.self, from: data) {
                onSuccess(baseLeague)
            } else {
                onError(NetworkError(title: "Ошибка!", message: "Произошла неизвестная ошибка", code: -1))
            }
        } onError: { error in
            onError(error)
        }
    }
    
    func getSeasonsById(_ id: String,
                        onSuccess: @escaping ((BaseSeason) -> Void),
                        onError: @escaping ((NetworkError) -> Void)) {
        let route = LeaguesRouter.seasonsByid(id: id)
        request(with: route.asURLRequest()) { data in
            if let baseSeasons = try? JSONDecoder().decode(BaseSeason.self, from: data) {
                onSuccess(baseSeasons)
            } else {
                onError(NetworkError(title: "Ошибка!", message: "Произошла неизвестная ошибка", code: -1))
            }
        } onError: { error in
            onError(error)
        }
    }
}
