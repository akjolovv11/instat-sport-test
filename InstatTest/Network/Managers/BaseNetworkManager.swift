//
//  BaseNetworkManager.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class BaseNetworkManager {
    
    func request(with request: URLRequest,
                 onSuccess: @escaping ((Data) -> Void),
                 onError: @escaping ((NetworkError) -> Void)) {
        
        URLSession.shared.dataTask(with: request) { data, _, error in
            if let data = data {
                onSuccess(data)
            } else {
                onError(NetworkError(error))
            }
        }
        .resume()
    }
    
    func request(with URL: URL,
                 onSuccess: @escaping ((Data) -> Void),
                 onError: @escaping ((NetworkError) -> Void)) {
        
        URLSession.shared.dataTask(with: URL) { data, _, error in
            if let data = data {
                onSuccess(data)
            } else {
                onError(NetworkError(error))
            }
        }
        .resume()
    }
}
