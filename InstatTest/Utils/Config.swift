//
//  Config.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

class Config {

    static  let shared = Config()
    private init() {}

    var baseURL: String {
        return "https://api-football-standings.azharimm.site/"
    }
}
