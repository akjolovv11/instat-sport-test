//
//  Assembler.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

protocol Assembler: LeaguesAssembler,
                    SeasonsAssembler {
    
}

class AppAssembler: Assembler {}
