//
//  Leagues.swift
//  InstatTest
//
//  Created by Jones on 21/7/22.
//

import Foundation

protocol LeaguesAssembler {
    func resolve() -> AvailableLeaguesTableViewController
    func resolve() -> LeaguesPresenter
}

extension LeaguesAssembler {
    func resolve() -> AvailableLeaguesTableViewController {
        AvailableLeaguesTableViewController()
    }
    
    func resolve() -> LeaguesPresenter {
        LeaguesPresenter()
    }
}
