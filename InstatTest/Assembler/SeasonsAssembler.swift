//
//  SeasonsAssembler.swift
//  InstatTest
//
//  Created by Jones on 22/7/22.
//

import Foundation

protocol SeasonsAssembler {
    func resolve() -> SeasonLeagueTableViewController
    func resolve() -> SeasonsPresenter
}

extension SeasonsAssembler {
    func resolve() -> SeasonLeagueTableViewController {
        SeasonLeagueTableViewController()
    }
    
    func resolve() -> SeasonsPresenter {
        SeasonsPresenter()
    }
}
